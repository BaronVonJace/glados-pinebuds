GLaDOS inspired sound files for the Pine64 Pinebuds.

This pack assumes you are using the OpenPineBuds firmware.
Replace the files found in `OpenPineBuds/config/_default_cfg_src_/res/en`

Compile, flash and enjoy!
